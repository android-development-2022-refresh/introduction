---
title:  Using Third-Party and Course Sample Code
template: main-full.html
---

## Using Third-Party Code

When writing your assignments, you may copy small sections of code and other content you find online **if and only if** you attribute it **and** the code is freely copyable with a license that says it's ok to copy. Place a comment above the copied section stating where you found it and the license that applies.

## Using code from stackoverflow
If you're using code from https://stackoverflow.com, note that all code and content is licensed CREATIVE COMMONS – ATTRIBUTION – SHARE-ALIKE (CC-BY-SA 3.0) (see https://creativecommons.org/licenses/by-sa/3.0/us/).

!!! warning

    THIS IS A VIRAL LICENSE!!!

    This means that any code/content you copy forces you to make your project be licensed CC-BY-SA 3.0!!! This is nearly always a no-no with companies you work for and the government!

!!! note

    Some code or content _may_ contain additional licenses. For example, when Googlers write code snippets on stackoverflow, they'll add something like

    ```
    // Copyright 2022 Google LLC.
    // SPDX-License-Identifier: Apache-2.0
    ```

    at the top of their code to indicate that it's also licensed under the Apache 2.0 license. 

    In this case, it's ok to use the code as long as you adhere to the Apache 2.0 license.

For your assignment submissions in this course, copying any content from stackoverflow is fine.

In real life, unless the content has a more permissive license, such as Apache 2.0, **do not copy code from stackoverflow**!

To use stackoverflow safely:

   * Use “clean room” approach
   * Read and understand
   * Close the browser
   * Then write your code

## Using Course Sample Code

You may copy code from my class examples as well. Once again, you must attribute it. Add a comment at the top of your assignment submission source files that states that you're using sample code from the course.

Code from the course sample code repository is licensed Apache 2 (see the LICENSE-code.md file in the repository) and can be used/incorporated into your own projects with attribution. Note that this code is generally not well-tested and may contain bugs. If you find something that doesn't work, please let me know and I'll update the samples.

Code that I write live in face-to-face class or post in a discussion forum **is not** licensed for any use outside this class. You may not post or distribute any this code for any purpose other than writing your assignments.
