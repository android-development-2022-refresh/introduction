---
title:  Assignments
template: main-full.html
---

## Late Submissions

Late submissions will be reduced by one letter grade (1 point) for each _**DAY**_ (or partial day) late (no exceptions without timely prior coordination with the instructors). See the grading policy for assignment grading details.

!!! note

    If you expect any issues with turning in work on time, please see the instructor as soon as you know. If I know in advance about travel, expected high workloads or other issues, I can work with you. However, letting me know a day or two before an assignment is due is not acceptable (If an emergency occurs, please let me know as soon as possible afterwards.)

!!! note

    If you encounter issues while trying to submit your assignment, please immediately send me an email with your submission text and a note stating that Canvas is being your best friend. If you submit in this manner, I will not count your submission as late.

Note that the easiest way to get a lower grade is to turn in your assignments late. If you know that something is coming up (business/vacation travel, expected very busy week at work, medical, expected baby) please let me know and I'm happy to work with you.

## Commit Often!!!

Note: I **highly** recommend that you store your code in a version control system such as subversion or git. However, if you host your project on a public site like github, you must make the repositories private! (Note that bitbucket provides free private repositories) Any non-private repositories that I find online will automatically set your grade to "F" for sharing code.

Be sure to control your source code! Lost code will not be accepted as an excuse for late or missing assignments.

## General guidelines for grade ranges

These are general guidelines, not absolute descriptions of a grading level. Your overall grade on an assignment depends on the overall quality and functionality of your submission. Again, these are **general** guidelines. You can get lower grades for very significant problems in your submissions and very late submissions.

| Letter Grade | Common Reasons for the Grade |
| ------------ | ---------------------------- |
| A            | On time AND<br/>Working AND<br/>Good design |
| B            | Missing required functionality<br/>Not quite working<br/>Bad design/bad style<br/>One day late (but would have otherwise been an "A") |
| C            | Many missing functions<br/>Will not compile<br/>Will not execute properly<br/>Very bad design<br/>One day late (but would have been an "B")<br/>Two days late (but would have been an "A") |
| F            | Little apparent effort<br/>Plagiarism<br/>Three or more days late<br/>Not turned in |

I grade on the following aspects of your submissions. Note that there is no specific percentage allocation for each of these concepts.

| Aspect | What I Look For |
| ------ | --------------- |
| Design | Does the design use the patterns discussed in class properly?<br/>Does the design fit the problem?<br/>Is the program designed with a maintenance programmer in mind?<br/>Can I determine why you did things the way you did by reading code and comments? |
| Function | Is all required function present?<br/>Is any non-required function present? (grade deduction)<br/>Does present function work properly? |
| Style and Coding Conventions | Is the code readable?<br/>Can I easily figure out what it's doing just by reading the code?<br/>Does the code follow the required coding conventions?<br/>Note: Not following the required coding conventions, even once in a submission, maximizes your assignment grade at A- |
| Timeliness | Was the assignment submitted on time?<br/>one day late = 1-point grade deduction<br/>two days late = 2-point grade deduction<br/>... |

## Coding Conventions

All assignments must observe the following coding conventions.

Why? I read your code

Sometimes your code doesn't work correctly (sometimes spectacularly so...). Before I decide on the grade to give you, I carefully read the code to try to figure out why it didn't work. If the reason is a small, subtle thing, I'll often give more credit (rather than simply saying "doesn't work; you get a C") I often have to do this for  several assignments. If the code is readable, using meaning variable, method and class names, I can often find the problems more quickly.

Keep in mind that I have many submissions to review each time, and usually 20% or more have some big issues. This also helps a good bit when some of you are having trouble before submitting and ask for help. If I can read the code, I can figure out what's going on and respond that much more quickly.

   * Indentation must be consistent. Use either spaces or tabs, but not both!

   * All type names must follow Upper-Camel Case
      * ShoppingCart
      * ObjectDrawingApplet

   * All variable and method names must follow Lower-Camel Case
      * drawObject()
      * numberOfObjectsOnScreen

   * Type, variable and method names shall be a series of full words, not abbreviations or single letters.
      * Standard acronyms are acceptable (such as url), but names like "c" are generally not acceptable. If you aren't sure whether a name is acceptable, feel free to ask me, but remember the rule of thumb: it should sound exactly like what it's being used for.
      * Some Exceptions (based on common, understood usage)
         * integer counters in loops can be named i, j, k
         * the "current number" for walking through an array or counting items can be n
         * e for an exception in a catch block

   * All classes and interfaces must be contained in a Kotlin package

   * All Kotlin package names must be completely lower-case and start with lastname.firstname.hw# where the # is the homework number. For example, I might have a project named "stanchfield.scott.hw4"(with the same Android package name) that contains Kotlin packages:

      ```
      stanchfield.scott.hw4
      stanchfield.scott.hw4.model
      stanchfield.scott.hw4.database
      ```

   * All projects must be named HW1, HW2, HW3 and so forth

   * All submission zip files must be named _lastname_._firstname_.HW1.zip, _lastname_._firstname_.HW2.zip, etc

   * All string literals that would appear for the user (typically text in TextViews, dialogs, toasts, etc) must be externalized into the strings.xml file. This is a really good habit to get into upfront, and you should always do this in any application you create to make localization simpler.

!!! note

    This means all user-facing text. Any string constants that the user could see, whether used in your XML files or Kotlin code, must be externalized.