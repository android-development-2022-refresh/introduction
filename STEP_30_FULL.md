---
title:  Grading
template: main-full.html
---

## Points

Assignments are worth 4 points each, mapping to GPA letters.

My grader and I assign letter grades first, then convert them to numbers and apply late penalties. The grading scale from letters to numbers is as follows:

| Letter Grade    | Numerical Equivalent  |
| --------------- | --------------------- |
| A               | 4.0                   |
| A-              | 3.7                   |
| (A/B border)    | 3.5                   |
| B+              | 3.3                   |
| B               | 3.0                   |
| B-              | 2.7                   |
| (B/C border)    | 2.5                   |
| C+              | 2.3                   |
| C               | 2.0                   |
| C-              | 1.7                   |
| (C/D border)    | 1.5                   |
| D+              | 1.3                   |
| D               | 1.0                   |
| D-              | 0.7                   |
| F               | 0                     |

Note that there is no A+. We had been reserving that for "exemplary" submissions, but some students considered an "A" as lost points.

Your final grade **WILL NOT** include +/- designations. A/A- both count as "A" and "4" for University GPA purposes(and similar for other letter grades).

## Late Penalty

Each *day* late results in 1 full letter grade (1 point) off. This is to prevent cascading lateness. In the past students would submit an assignment a week late and have one week less to do the next assignment, which would often result in that assignment being late as well. The also made it difficult to discus assignments until all were submitted.

When grading assignments, we will write comments describing your grade, but we will usually not note specific point-value deductions (see "Holistic Grading" below). Some comments are "for your information"/"helpful hints" and will be marked to indicate that they have no effect on the grade.

If you disagree with a grade you can ask us to review it. Tell us what your concerns are and we'll look at it. Sometimes we may miss something in your code.

If we note a problem with a submission that was also noted for a previous submission, we will not count off for it. (If we do, please let use know and we'll correct the grade.)

## "Holistic" Grading

I started out using a rubric to grade. For many reasons, this almost never reflected reality, so after a few terms, I realized a much fairer scheme, that I call "holistic grading"

First, some problems with Rubrics...

   * Gaming the System

      When I had point counts next to features, many students would decide which features to implement based on how many points they felt they could afford to lose. Rather than implement all function (the point of which is to exercise important techniques), they would implement a subset just to "get the grade". This is frustrating for a teacher because we design exercises to be sure students have had the opportunity to try out important techniques.

   * "Sum of the Parts" vs "The Whole"
    
      Rubrics tend to cut both ways when the points are added up...

      Sometimes, a few relatively minor things that don't greatly impact the overall submission would numerically push the grade into "B" territory or lower. This didn't feel fair to the student based on the overall result, but to be fair to all students I had to stick with the grade based off the rubric.

      Other times, small things that made an impact when combined could result in an "A" that didn't feel right (sometimes because of gaming the system, other times because the small problems really added up).

   * "That shouldn't be n points off"

      The most common complaint would be that either the rubric was unfair in the number of points it assigned to a specific item, or that I was applying a rubric item too broadly to a problem in the submission. Most of the time I would leave the grade as-is, but in some cases it was a matter of a possible alternative interpretation of the assignment requirements, in which case I adjusted the points off.

      Later, after I stopped using rubrics, but would list points deductions on assignment comments, I'd get the same complaints.

      In either case, this never made any significant difference in the grade for the assignment, or the course.

Students ended up spending a good bit of time trying to figure out which items they could argue to get a few points back, not realizing a few points over the term made no effective difference.

This led me to... "Holistic Grading"

(If it sounds a little "New Age", it's not... "Holistic" here simply means "looking at the whole")

"This feels like a B+"...

My grader and I write comments about what we see in the assignment, and don't write specific deductions for each one.

We look at the overall result, taking all comments into account, and say "that's an A", or "that's a B-", or "that's barely anything more than the sample code" (at which point we estimate the % of the assignment that was done and assign that number).

The letters we assign are converted to numbers. A=4.0, A-=3.7, etc.

When coming up with these letters, one of my key concepts is "things that cap the grade at A-". There's not a fixed list of such items, but things like "coding conventions" fall into this category. If several small things like these happen, a rubric may have pushed the grade into "B" territory. When I see several little things but otherwise the overall submission feels like an "A", I use "A-" as the grade. Note that if there are "cap" items as well as bigger issues, the "cap" items won't push the grade lower than the bigger issues already would have.

Usually, if features are missing or not implemented per the assignment description, I start thinking "B", but if they're very minor features or present but not quite what was explicitly stated in the assignment description, and otherwise the assignment looked good, I'll go with "A-".

Because of this, "A-" is often the most common grade for an assignment.

If the assignment does everything we ask, in the ways I taught in class, meets coding standards, and is on time, that's where "A" comes in.

Other grades (before late deductions) are usually driven by

   * Feature omissions or problems

      Most common: something is missing or doesn't work properly

      This is often because a student started work a few days before the assignment was due and didn't have time to ask questions or study the concepts.

   * Crashes and compilation errors

      Note that we try to make your code work before grading it. Sometimes it's an inverted boolean expression, and if we can get it working and it's something simple, we'll give a better grade than "doesn't work".

      However, sometimes, after spending an hour or two trying to get it to work, we have to give up...

   * Working together or Plagiarism

      I've caught _many_ students doing this...

      Sometimes they even try to submit something they found online that doesn't match the assignment description or is very obviously not based on what I've taught.

Bottom line: When you see a "3.7" for a submission, don't think of it as "0.3 points off". Think of it as a few minor things aren't right. If that's how most of your assignments go, that's an "A" for the course...

## Distribution of Grades

It is possible (and has happened in past sections) for everyone to get an "A" in this course. I will not be applying any sort of curve or other means to artificially distribute grades.

I will be looking closely at your efforts in this class when determining your final grades. An "A" grade in this course requires a good deal of effort. Do not expect an "A" if you start programming a few nights before they are due...

Let me repeat that. Do not expect an "A" if you start programming assignments a few nights before they are due... This has often been the reason for B and C grades in the course; students do not allow enough time to ask clarifying questions or do the coding.

Be sure to at least look over and understand the assignments at the beginning of the week they are assigned! This will give you time to ask questions.

If you do all the work well and on-time, you will get an "A" for the course. If you do all of the work on-time but are having trouble with the concepts, you'll likely get a "B" for the course. If you're in danger of a lower grade, I will speak with you as soon as it becomes apparent to me.