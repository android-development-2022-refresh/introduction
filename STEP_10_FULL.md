---
title:  Course Hardware and Software
template: main-full.html
---

## Hardware

You do not need an Android device for your coursework, but if you have access to one (or want to
purchase one for your coursework) development feels more realistic. Note that if you have low memory
and/or do not have an SSD on your computer, an external device may help performance.

For running Android Studio, the minimum recommended configuration is 8GB RAM with an SSD. More memory is better, as Android Studio and the Android emulator love to use lots of memory. Ideally you'll have 16GB or more RAM.

If you have only 8GB RAM, I strongly recommend you either upgrade your RAM or purchase an inexpensive Android phone for classwork. Many pay-as-you-go phones (which you do not have to activate) can be found for under $100 and will greatly improve your development experience. (Note, however, a physical device is _not_ required for this course.)

## Software

The following are the required versions of tools for this term. 

Please keep a copy of all downloaded installation files in case you need to reinstall during the term!

!!! warning 

    Do not upgrade any versions after the first module unless I instruct you to do so! We want
    to make sure we're using the same version of everything that you are when we grade
    your assignment submissions.

| Name                                                                              | Version                    |
| --------------------------------------------------------------------------------- | -------------------------- |
| Android Studio<br/>https://developer.android.com/studio#android-studio-downloads  | Electric Eel (2022.1.1.19) |
| Kotlin<br/>(kotlin_version in root build.gradle)                                  | 1.7.20                     |
| Jetpack Compose Bill of Materials<br/>(compose_bom_version in root build.gradle)  | 2023.01.00                 |
| Jetpack Compose Compiler<br/>(compose_compiler_version in root build.gradle)      | 1.3.2                      |
| Android minimum API<br/>(minSdk in app/build.gradle)                              | 24                         |
| Android target API<br/>(targetSdk in app/build.gradle)                            | 33                         |
| Android compile API<br/>(compileSdk in app/build.gradle)                          | 33                         |

## Sample Code

See the "Sample Code" module for download and usage instructions

If you see any problems in the sample code, please let me know and I'll update it.
